//
//  VertexArray.hh
//  ergl
//
//  Created by Eryn Wells on 9/13/15.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#pragma once

#include <memory>
#include <vector>

#include "BufferObject.hh"
#include "Object.hh"


namespace ergl {

struct VertexArray
    : public Object
{
    typedef std::shared_ptr<VertexArray> Ptr;

    VertexArray();
    virtual ~VertexArray();

    void bind() const;
    void unbind() const;

    void addVertexBuffer(VertexBuffer::Ptr buffer);

private:
    std::vector<VertexBuffer::Ptr> mVertexBuffers;
};

}
