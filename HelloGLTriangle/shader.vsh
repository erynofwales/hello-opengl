#version 410

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexColor;

uniform mat4 perspectiveMatrix;

out vec3 color;

void main()
{
    color = vertexColor;
    gl_Position = perspectiveMatrix * vec4(vertexPosition, 1.0);
}