//
//  main.m
//  HelloGLTriangle
//
//  Created by Eryn Wells on 2015-09-10.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
