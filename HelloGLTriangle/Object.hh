//
//  Object.hh
//  ergl
//
//  Created by Eryn Wells on 2015-09-11.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#pragma once

#include <memory>

#include <OpenGL/gl3.h>


namespace ergl {

/**
 * Base class wrapping OpenGL objects. All objects (to my knowledge) are given
 * object IDs by OpenGL when they are created. This class holds on to the ID for
 * the object.
 */
struct Object
{
    typedef std::shared_ptr<Object> Ptr;

    /** Destructor. */
    virtual ~Object();

protected:
    GLuint mObjectID;

    /**
     * Build an uninitialized object. This constructor is protected because it
     * shouldn't be called except in specific circumstances where OpenGL
     * requires the address of the mObjectID variable to be passed in for object
     * creation.
     */
    Object();

    /**
     * Builds an Object with the given objectID.
     *
     * @param [in] objectID     The ID of the object, as provided by OpenGL.
     */
    Object(GLuint objectID);

    /**
     * Reset the object ID to a default value. Subclasses should only call this
     * in case of unrecoverable errors.
     */
    void resetObjectID();
};

}
