//
//  Shader.hh
//  HelloGLTriangle
//
//  Created by Eryn Wells on 2015-09-11.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#pragma once

#include <memory>

#include <OpenGL/gl3.h>

#include "Object.hh"


namespace ergl {

/**
 * A C++ object that wraps an OpenGL shader. Shaders are created and initialized
 * with OpenGL on object construction and destroyed on object destruction.
 */
struct Shader
    : Object
{
    typedef std::shared_ptr<Shader> Ptr;

    /**
     * Make a vertex shader from the given code.
     *
     * @param [in] code     The source of the shader.
     * @return  A pointer to the new shader.
     */
    static Shader::Ptr makeVertexShader(const std::string &code);

    /**
     * Make a fragment shader from the given code.
     *
     * @param [in] code     The source of the shader.
     * @return  A pointer to the new shader.
     */
    static Shader::Ptr makeFragmentShader(const std::string &code);

    /**
     * Builds a shader object from the given string containing the source of the
     * shader.
     *
     * @param [in] code The source of the shader.
     * @param [in] type The type of the shader. Must be one of the recognized
     *                  OpenGL shader types.
     */
    Shader(const std::string &code, GLenum type);

    /**
     * Builds a shader object with the given input stream containing the source
     * of the shader.
     *
     * @param [in] infile   An input stream containing the source of the shader.
     * @param [in] type     The type of the shader. Must be one of the
     *                      recognized OpenGL shader types.
     * @see Shader::Shader
     */
    Shader(std::istream &infile, GLenum type);

    /** Destructor. Destroys the shader with OpenGL if necessary. */
    virtual ~Shader();

    /**
     * Attach this shader to a program with the given ID.
     *
     * @param [in] programID    The OpenGL object ID of the program to attach
     *                          this shader to.
     */
    void attachToProgramWithID(GLuint programID) const;

    /**
     * Detach this shader from a program with the given ID.
     *
     * @param [in] programID    The OpenGL object ID of the program to detach
     *                          this shader from.
     */
    void detachFromProgramWithID(GLuint programID) const;

private:
    /**
     * Retrieve the info log message about the shader from OpenGL.
     *
     * @return  A string containing the log message, or an empty string if no
     *          message was present.
     */
    std::string getInfoLogMessage() const;

    /** Copy constructor. Copying is not allowed. */
    Shader(const Shader &other);

    /** Assignment operator. Assignment is not allowed. */
    Shader &operator=(const Shader &other);
};

}
