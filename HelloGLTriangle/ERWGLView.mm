//
//  ERWGLView.m
//  HelloGLTriangle
//
//  Created by Eryn Wells on 2015-09-10.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#include <string>

#import <OpenGL/gl3.h>
#import <GLKit/GLKMath.h>

#import "ERWGLView.h"

#import "BufferObject.hh"
#import "Program.hh"
#import "Shader.hh"
#import "VertexArray.hh"


static NSString *strVertexShader = @"#version 330\n"
                                    "layout(location = 0) in vec4 position;\n"
                                    "void main()\n"
                                    "{\n"
                                    "   gl_Position = position;\n"
                                    "}\n";

static NSString *strFragmentShader = @"#version 330\n"
                                      "out vec4 outputColor;\n"
                                      "void main()\n"
                                      "{\n"
                                      "   outputColor = vec4(1.0, 1.0, 1.0, 1.0);\n"
                                      "}\n";


static CVReturn
displayLinkCallback(CVDisplayLinkRef displayLink,
                    const CVTimeStamp *now, const CVTimeStamp *outputTime,
                    CVOptionFlags flagsIn, CVOptionFlags *flagsOut,
                    void *context);

@interface ERWGLView ()

@property (assign, nonatomic) CVDisplayLinkRef displayLink;

@property (assign, nonatomic) int rotationProgress;
@property (assign, nonatomic) int rotationPoleProgress;
@property (assign, nonatomic) GLuint vertexBuffer;
@property (assign, nonatomic) GLuint colorBuffer;
@property (assign, nonatomic) ergl::VertexArray::Ptr vao;
@property (assign, nonatomic) ergl::Program::Ptr program;
@property (assign, nonatomic) GLuint currentShader;

@end


@implementation ERWGLView

- (void)awakeFromNib
{
    NSOpenGLPixelFormatAttribute attrs[] = {
        NSOpenGLPFAAccelerated,
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFADepthSize, 24,
        NSOpenGLPFAOpenGLProfile,
        NSOpenGLProfileVersion4_1Core,
        0
    };

    NSOpenGLPixelFormat *pf = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
    NSOpenGLContext *context = [[NSOpenGLContext alloc] initWithFormat:pf shareContext:nil];

    CGLEnable([context CGLContextObj], kCGLCECrashOnRemovedFunctions);

    self.pixelFormat = pf;
    self.openGLContext = context;

    [self setWantsBestResolutionOpenGLSurface:YES];
}

- (void)prepareOpenGL
{
    [super prepareOpenGL];

    NSError *error = nil;
    NSString *vertexShaderCode = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"shader" ofType:@"vsh"] encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        assert(NO);
    }
    NSString *fragmentShaderCode = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"shader" ofType:@"fsh"] encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        assert(NO);
    }
    std::vector<ergl::Shader::Ptr> shaders;
    shaders.emplace_back(ergl::Shader::makeVertexShader(vertexShaderCode.UTF8String));
    shaders.emplace_back(ergl::Shader::makeFragmentShader(fragmentShaderCode.UTF8String));

    self.program = std::make_shared<ergl::Program>(shaders);

#define A -0.5,  0.5,  0.5
#define B -0.5, -0.5,  0.5
#define C  0.5, -0.5,  0.5
#define D  0.5,  0.5,  0.5
#define E -0.5,  0.5, -0.5
#define F -0.5, -0.5, -0.5
#define G  0.5, -0.5, -0.5
#define H  0.5,  0.5, -0.5

    static const std::vector<float> vertexPositions {
        A, B, C, C, D, A, // front
        H, G, F, F, E, H, // back
        E, A, D, D, H, E, // top
        B, F, G, G, C, B, // bottom
        E, F, B, B, A, E, // left
        D, C, G, G, H, D, // right
    };

#undef A
#undef B
#undef C
#undef D
#undef E
#undef F
#undef G
#undef H

#define Gr 0.0, 1.0, 0.0
#define Or 1.0, 0.5, 0.0
#define Rd 1.0, 0.0, 0.0
#define Yw 1.0, 1.0, 0.0
#define Bl 0.0, 0.0, 1.0
#define Mg 1.0, 0.0, 1.0

    static const std::vector<float> vertexColors {
        Gr, Gr, Gr, Gr, Gr, Gr,
        Or, Or, Or, Or, Or, Or,
        Rd, Rd, Rd, Rd, Rd, Rd,
        Yw, Yw, Yw, Yw, Yw, Yw,
        Bl, Bl, Bl, Bl, Bl, Bl,
        Mg, Mg, Mg, Mg, Mg, Mg,
    };

#undef Gr
#undef Or
#undef Rd
#undef Yw
#undef Bl
#undef Mg

    self.vao = std::make_shared<ergl::VertexArray>();
    self.vao->bind();

    ergl::VertexBuffer::Ptr vertexBuffer = std::make_shared<ergl::VertexBuffer>();
    vertexBuffer->bind();
    vertexBuffer->setData(vertexPositions, ergl::VertexBuffer::Dimension::Three, 0, GL_STATIC_DRAW);
    self.vao->addVertexBuffer(vertexBuffer);

    ergl::VertexBuffer::Ptr colorBuffer = std::make_shared<ergl::VertexBuffer>();
    colorBuffer->bind();
    colorBuffer->setData(vertexColors, ergl::VertexBuffer::Dimension::Three, 0, GL_STATIC_DRAW);
    self.vao->addVertexBuffer(colorBuffer);

    glClearColor(0.16, 0.17, 0.21, 1);

    glEnable(GL_DEPTH_TEST);
    //glClearDepth(1.0f);
    glDepthFunc(GL_LESS);

    [self prepareDisplayLink];
}

- (void)prepareDisplayLink
{
    GLint swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    /* Create a display link capable of being used with active displays. */
    CVDisplayLinkCreateWithActiveCGDisplays(&_displayLink);

    /* Set the renderer output callback function. */
    CVDisplayLinkSetOutputCallback(self.displayLink, &displayLinkCallback, (__bridge void *)(self));

    /* Set display link for the current renderer. */
    CGLContextObj cglContext = [[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = [[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(self.displayLink, cglContext, cglPixelFormat);

    /* Start up the display link. */
    CVDisplayLinkStart(self.displayLink);
}

- (CVReturn)getFrameForTime:(const CVTimeStamp *)outputTime
{
    NSOpenGLContext *context = [self openGLContext];
    [context makeCurrentContext];
    CGLLockContext((CGLContextObj)[context CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, self.bounds.size.width, self.bounds.size.height);

    self.program->activate();

    GLKMatrix4 projection = GLKMatrix4MakePerspective(45.0, self.bounds.size.width / self.bounds.size.height, 0.1f, 1000.0f);
    GLKMatrix4 view = GLKMatrix4MakeLookAt(0.0, 1.5, 1.5,
                                           0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0);
    GLKVector2 rotationPole = [self rotationPoleVector];
    GLKMatrix4 model = GLKMatrix4MakeRotation([self rotationAngle], rotationPole.x, rotationPole.y, 0.0);
    GLKMatrix4 perspectiveMatrix = GLKMatrix4Multiply(GLKMatrix4Multiply(projection, view), model);

    self.program->setUniform("perspectiveMatrix", perspectiveMatrix.m);

    self.vao->bind();
    glDrawArrays(GL_TRIANGLES, 0, 12 * 3);
    self.vao->unbind();

    self.program->deactivate();

    [context flushBuffer];
    CGLUnlockContext((CGLContextObj)[context CGLContextObj]);

    self.rotationProgress = (self.rotationProgress + 1) % 180;
    self.rotationPoleProgress = (self.rotationPoleProgress + 1) % 360;

    return kCVReturnSuccess;
}

- (GLfloat)rotationAngle
{
    return self.rotationProgress / 180.0f * 2.0f * M_PI;
}

- (GLKVector2)rotationPoleVector
{
    return GLKVector2Make(cosf(self.rotationPoleProgress / 360.0 * 2.0 * M_PI),
                          sinf(self.rotationPoleProgress / 360.0 * 2.0 * M_PI));
}

@end

CVReturn
displayLinkCallback(CVDisplayLinkRef displayLink,
                    const CVTimeStamp *now, const CVTimeStamp *outputTime,
                    CVOptionFlags flagsIn, CVOptionFlags *flagsOut,
                    void *context)
{
    CVReturn result;
    @autoreleasepool {
        result = [(__bridge ERWGLView *)context getFrameForTime:outputTime];
    }
    return result;
}