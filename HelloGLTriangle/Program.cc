//
//  Program.cc
//  ergl
//
//  Created by Eryn Wells on 2015-09-11.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#include <stdexcept>
#include <string>

#include "Program.hh"


namespace ergl {

Program::Program(const std::vector<Shader::Ptr> &shaders)
    : Object(glCreateProgram())
{
    if (mObjectID == 0) {
        throw std::runtime_error("Couldn't create program");
    }

    if (shaders.size() <= 0) {
        destroy();
        throw std::runtime_error("No shaders provided to program");
    }

    for (const Shader::Ptr &shader : shaders) {
        shader->attachToProgramWithID(mObjectID);
    }

    glLinkProgram(mObjectID);

    for (const Shader::Ptr &shader : shaders) {
        shader->detachFromProgramWithID(mObjectID);
    }

    GLint status;
    glGetProgramiv(mObjectID, GL_LINK_STATUS, &status);
    if (status == GL_FALSE) {
        std::string msg("Error linking program: " + getInfoLogMessage());
        destroy();
        throw std::runtime_error(msg);
    }
}

Program::~Program()
{
    if (mObjectID != 0) {
        destroy();
    }
}

void
Program::activate()
    const
{
    glUseProgram(mObjectID);
}

void
Program::deactivate()
    const
{
    glUseProgram(0);
}

void
Program::setUniform(const std::string &name,
                    GLfloat matrix[16],
                    bool transpose)
    const
{
    GLint uniform = getUniformID(name);
    glUniformMatrix4fv(uniform, 1, transpose ? GL_TRUE : GL_FALSE, matrix);
}

void
Program::destroy()
{
    glDeleteProgram(mObjectID);
    resetObjectID();
}

GLint
Program::getUniformID(const std::string &name)
    const
{
    GLint id = glGetUniformLocation(mObjectID, name.c_str());
    if (id < 0) {
        throw std::runtime_error("No uniform named '" + name + "' in program " + std::to_string(mObjectID));
    }
    return id;
}

std::string
Program::getInfoLogMessage()
    const
{
    GLint infoLogLen;
    glGetProgramiv(mObjectID, GL_INFO_LOG_LENGTH, &infoLogLen);
    char *infoLogMsg = new char[infoLogLen + 1];
    glGetProgramInfoLog(mObjectID, infoLogLen, NULL, infoLogMsg);
    std::string msg(infoLogMsg);
    delete[] infoLogMsg;
    return msg;
}

}
