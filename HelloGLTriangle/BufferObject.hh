//
//  BufferObject.hh
//  ergl
//
//  Created by Eryn Wells on 9/12/15.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#pragma once

#include <vector>

#include "Object.hh"


namespace ergl {

struct Buffer
    : public Object
{
    virtual ~Buffer();

    GLenum type() const noexcept;

    virtual void bind() const = 0;
    virtual void unbind() const = 0;

protected:
    Buffer();

    void setData(GLenum type, const std::vector<float> &data, GLenum usage);

private:
    GLenum mType;
};


struct VertexBuffer
    : public Buffer
{
    typedef std::shared_ptr<VertexBuffer> Ptr;

    enum class Dimension {
        One = 1,
        Two,
        Three,
        Four
    };

    virtual ~VertexBuffer();

    Dimension dimension() const noexcept;
    GLsizei stride() const noexcept;

    void setData(const std::vector<float> &data, Dimension dimension, int stride, GLenum usage);

    virtual void bind() const;
    virtual void unbind() const;

private:
    /**
     * The dimension of the vertex data. Each vertex must have exactly this
     * many of components.
     */
    Dimension mDimension;

    /** The byte offset between each vertex. */
    GLsizei mStride;
};

}
