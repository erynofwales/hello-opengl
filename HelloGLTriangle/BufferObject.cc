//
//  BufferObject.cc
//  ergl
//
//  Created by Eryn Wells on 9/12/15.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#include <stdexcept>
#include <string>

#include <OpenGL/gl3.h>

#include "BufferObject.hh"


namespace ergl {

#pragma mark - Buffer

Buffer::Buffer()
    : Object()
{
    glGenBuffers(1, &mObjectID);
}

Buffer::~Buffer()
{ }

GLenum
Buffer::type()
    const noexcept
{
    return mType;
}

void
Buffer::setData(GLenum type,
                const std::vector<float> &data,
                GLenum usage)
{
    mType = GL_FLOAT;
    glBufferData(type, sizeof(float) * data.size(), data.data(), usage);
}

#pragma mark - VertexBuffer

VertexBuffer::~VertexBuffer()
{
    if (mObjectID) {
        glDeleteBuffers(1, &mObjectID);
        resetObjectID();
    }
}

VertexBuffer::Dimension
VertexBuffer::dimension()
    const noexcept
{
    return mDimension;
}

GLsizei
VertexBuffer::stride()
    const noexcept
{
    return mStride;
}

void
VertexBuffer::setData(const std::vector<float> &data,
                      Dimension dimension,
                      int stride,
                      GLenum usage)
{
    mDimension = dimension;
    mStride = stride;
    if ((data.size() % static_cast<int>(dimension)) != 0) {
        throw std::runtime_error("Vertex buffer data is not an even multiple its stride attribute");
    }
    Buffer::setData(GL_ARRAY_BUFFER, data, usage);
}

void
VertexBuffer::bind()
    const
{
    glBindBuffer(GL_ARRAY_BUFFER, mObjectID);
}

void
VertexBuffer::unbind()
    const
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

}
