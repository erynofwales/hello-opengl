//
//  VertexArray.cc
//  ergl
//
//  Created by Eryn Wells on 9/13/15.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#include "VertexArray.hh"


using namespace ergl;


VertexArray::VertexArray()
    : Object(),
      mVertexBuffers()
{
    glGenVertexArrays(1, &mObjectID);
    // TODO: Check for failed allocation.
}

VertexArray::~VertexArray()
{
    if (mObjectID) {
        glDeleteVertexArrays(1, &mObjectID);
        resetObjectID();
    }
}

void
VertexArray::bind()
    const
{
    glBindVertexArray(mObjectID);
}

void
VertexArray::unbind()
    const
{
    glBindVertexArray(0);
}

void
VertexArray::addVertexBuffer(VertexBuffer::Ptr buffer)
{
    GLuint index = static_cast<GLuint>(mVertexBuffers.size());
    glVertexAttribPointer(index,
                          static_cast<GLint>(buffer->dimension()),
                          buffer->type(),
                          GL_FALSE,
                          buffer->stride(),
                          NULL);
    // TODO: Is there any reason *not* to do this?
    glEnableVertexAttribArray(index);
    mVertexBuffers.push_back(buffer);
}
