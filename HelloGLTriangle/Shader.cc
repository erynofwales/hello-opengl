//
//  Shader.cc
//  HelloGLTriangle
//
//  Created by Eryn Wells on 2015-09-11.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#include <istream>
#include <stdexcept>
#include <string>

#include "Shader.hh"


namespace ergl {

/* static */ Shader::Ptr
Shader::makeVertexShader(const std::string &code)
{
    return std::make_shared<Shader>(code, GL_VERTEX_SHADER);
}

/* static */ Shader::Ptr
Shader::makeFragmentShader(const std::string &code)
{
    return std::make_shared<Shader>(code, GL_FRAGMENT_SHADER);
}

Shader::Shader(const std::string &code,
               GLenum type)
    : Object(glCreateShader(type))
{
    if (mObjectID == 0) {
        throw std::runtime_error("Couldn't create shader object");
    }

    const char *codeStr = code.c_str();
    glShaderSource(mObjectID, 1, (const GLchar **)&codeStr, NULL);

    // Compile the shader.
    glCompileShader(mObjectID);

    // Check for a compile error.
    GLint status;
    glGetShaderiv(mObjectID, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        std::string msg("Compile error in shader: " + getInfoLogMessage());
        glDeleteShader(mObjectID);
        resetObjectID();
        throw std::runtime_error(msg);
    }
}

Shader::Shader(std::istream &infile,
               GLenum type)
      // Read the stream into a string and call the designated constructor.
    : Shader(std::string(std::istreambuf_iterator<char>(infile), {}), type)
{ }

Shader::~Shader()
{
    if (mObjectID) {
        glDeleteShader(mObjectID);
        resetObjectID();
    }
}

void
Shader::attachToProgramWithID(GLuint programID)
    const
{
    glAttachShader(programID, mObjectID);
}

void
Shader::detachFromProgramWithID(GLuint programID)
    const
{
    glDetachShader(programID, mObjectID);
}

std::string
Shader::getInfoLogMessage()
    const
{
    std::string msg;
    GLint infoLogLen;
    glGetShaderiv(mObjectID, GL_INFO_LOG_LENGTH, &infoLogLen);
    char *infoLogMsg = new char[infoLogLen + 1];
    glGetShaderInfoLog(mObjectID, infoLogLen, NULL, infoLogMsg);
    msg += infoLogMsg;
    delete[] infoLogMsg;
    return msg;
}

}
