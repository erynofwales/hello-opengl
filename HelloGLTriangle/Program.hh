//
//  Program.hh
//  ergl
//
//  Created by Eryn Wells on 2015-09-11.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#pragma once

#include <memory>
#include <vector>

#include "Object.hh"
#include "Shader.hh"


namespace ergl {

/**
 * A Program in OpenGL is a set of shaders that perform a specific task. This
 * class encapsulates an OpenGL program, and manages compiling and linking, and
 * making sure it is ready to use.
 */
struct Program
    : Object
{
    typedef std::shared_ptr<Program> Ptr;

    /**
     * Build a program object from the given list of shaders. It is an error to
     * provide a list containing no shaders.
     *
     * @param [in] shaders  A list of shaders from which to build the program.
     */
    Program(const std::vector<Shader::Ptr> &shaders);

    /** Destructor. Destroys the program with OpenGL if necessary. */
    virtual ~Program();

    /** Tell OpenGL to use this program for rendering. */
    void activate() const;

    /** Stop using this program for rendering. */
    void deactivate() const;

    /**
     * Set the value of a single mat4 uniform defined by this program.
     *
     * @param [in] name         The name of the uniform to set.
     * @param [in] matrix       The matrix to set.
     * @param [in] transpose    Flag indicating whether this matrix is transposed.
     */
    void setUniform(const std::string &name, GLfloat matrix[16], bool transpose = false) const;

private:
    /**
     * Unconditionally destroy the program with OpenGL. It is the caller's
     * responsibility to make sure this is a safe operation.
     */
    void destroy();

    /**
     * Get the ID of a uniform of the given name.
     *
     * @param [in] name     The name of the uniform.
     * @return The uniform ID.
     * @throws std::runtime_error   If the uniform is not defined by this program.
     */
    GLint getUniformID(const std::string &name) const;

    /**
     * Retrieve the info log message about the program from OpenGL.
     *
     * @return  A string containing the log message, or an empty string if no
     *          message was present.
     */
    std::string getInfoLogMessage() const;
};

}
