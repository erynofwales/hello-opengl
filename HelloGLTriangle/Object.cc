//
//  Object.cc
//  ergl
//
//  Created by Eryn Wells on 2015-09-11.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#include "Object.hh"

namespace ergl {

Object::Object()
    : Object(0)
{ }

Object::Object(GLuint objectID)
    : mObjectID(objectID)
{ }

Object::~Object()
{ }

void
Object::resetObjectID()
{
    mObjectID = 0;
}

}